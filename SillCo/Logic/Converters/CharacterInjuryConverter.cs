﻿using SillCo.Models.Characters;
using SillCoDB;
using System.Collections.Generic;

namespace SillCo.Data.Converters
{
    public class CharacterInjuryConverter
    {
        public List<CharacterInjuryModel> ConvertToInjuryModel(List<Character_Injuries> dbInjuries)
        {
            List<CharacterInjuryModel> injuries = new List<CharacterInjuryModel>();
            foreach(var dbInj in dbInjuries)
            {
                var injury = new CharacterInjuryModel();
                injury.InjuryType = dbInj.injury_type;
                injury.Modifier = dbInj.modifier.Value;
                injury.IsHealed = false;
                injuries.Add(injury);
            }

            return injuries;
        }
    }
}