﻿using SillCo.Logic.Converters;
using SillCo.Models.Characters;
using SillCoDB;
using System;
using System.Collections.Generic;

namespace SillCo.Data.Converters
{
    public class CharacterSkillConverter
    {

        public static List<CharacterSkillModel> ConvertToModel(List<Character_Skills> characterSkills)
        {
            var cSkillModelList = new List<CharacterSkillModel>();

            CharacterSkillModel skillMod;
            foreach (Character_Skills cs in characterSkills)
            {
                skillMod = new CharacterSkillModel();
                skillMod.ID = cs.skill_id;
                skillMod.Name = cs.Skill.skill_name;
                skillMod.Attribute = cs.Skill.Attribute.name;
                skillMod.Skill = cs.skill_points.Value;
                skillMod.Complexity = cs.complexity.Value;
                skillMod.Specialization = cs.specialization;
                cSkillModelList.Add(skillMod);
            }
            return cSkillModelList;
        }

        public static List<Character_Skills> ConvertToDao(List<CharacterSkillModel> skills, int characterId)
        {
            var cs = new List<Character_Skills>();
            foreach (CharacterSkillModel skill in skills)
            {
                cs.Add(new Character_Skills()
                {
                    character_id = characterId,
                    skill_id = skill.ID,
                    skill_points = skill.Skill,
                    complexity = skill.Complexity,
                    specialization = skill.Specialization,
                    created_date = DateTime.Today,
                    last_modified = DateTime.Today,
                    modified_by = ConverterHelper.USERSTAMP
                });
            }
            return cs;
        }
    }
}