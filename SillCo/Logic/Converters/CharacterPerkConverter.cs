﻿using SillCo.Logic.Converters;
using SillCo.Models.Characters;
using SillCoDB;
using System;
using System.Collections.Generic;

namespace SillCo.Data.Converters
{
    public class CharacterPerkConverter
    {
        public static List<CharacterPerkModel> ConvertToModel(List<Character_Perks> dbPerkList)
        {
            List<CharacterPerkModel> perks = new List<CharacterPerkModel>();
            foreach (Character_Perks dbPerk in dbPerkList)
            {
                CharacterPerkModel perk = new CharacterPerkModel();
                perk.Name = dbPerk.Perk.name;
                perk.PerkId = dbPerk.perk_id;
                perk.Modifier = dbPerk.Perk.value.Value;
                perk.Desc = dbPerk.Perk.desc;
                perks.Add(perk);
            }

            return perks;
        }

        public static List<Character_Perks> ConvertToDao(List<CharacterPerkModel> perks, int characterId)
        {
            var cp = new List<Character_Perks>();
            foreach (CharacterPerkModel perk in perks)
            {
                cp.Add(new Character_Perks()
                {
                    character_id = characterId,
                    perk_id = perk.PerkId,
                    created_date = DateTime.Today,
                    last_modified = DateTime.Today,
                    modified_by = ConverterHelper.USERSTAMP
                });
            }
            return cp;
        }
    }
}