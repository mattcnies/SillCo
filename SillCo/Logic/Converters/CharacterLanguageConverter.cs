﻿using SillCo.Logic.Converters;
using SillCo.Models.Characters;
using SillCoDB;
using System;
using System.Collections.Generic;

namespace SillCo.Data.Converters
{
    public class CharacterLanguageConverter
    {
        public static List<CharacterLanguageModel> ConvertToLanguageModel(List<Character_Languages> dbLangs)
        {
            List<CharacterLanguageModel> langs = new List<CharacterLanguageModel>();
            foreach (Character_Languages dbLang in dbLangs)
            {
                CharacterLanguageModel lang = new CharacterLanguageModel();
                lang.LanguageId = dbLang.language_id;
                lang.Name = dbLang.Language.name;
                langs.Add(lang);
            }
            return langs;
        }

        public static List<Character_Languages> ConvertToDao(List<CharacterLanguageModel> langs, int characterId)
        {
            var cl = new List<Character_Languages>();
            foreach (CharacterLanguageModel lang in langs)
            {
                cl.Add(new Character_Languages()
                {
                    character_id = characterId,
                    language_id = lang.LanguageId,
                    created_date = DateTime.Today,
                    last_modified = DateTime.Today,
                    modified_by = ConverterHelper.USERSTAMP
                });
            }
            return cl;
        }
    }
}