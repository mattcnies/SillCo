﻿using SillCo.Logic.Converters;
using SillCo.Models.Characters;
using SillCoDB;
using System;
using System.Collections.Generic;

namespace SillCo.Data.Converters
{
    public class CharacterFlawConverter
    {
        public static List<CharacterFlawModel> ConvertToModel(List<Character_Flaws> dbFlawList)
        {
            List<CharacterFlawModel> flaws = new List<CharacterFlawModel>();
            foreach (Character_Flaws dbFlaw in dbFlawList)
            {
                CharacterFlawModel flaw = new CharacterFlawModel();
                flaw.Name = dbFlaw.Flaw.name;
                flaw.FlawId = dbFlaw.flaw_id;
                flaw.Modifier = dbFlaw.Flaw.value.Value;
                flaw.Desc = dbFlaw.Flaw.desc;
                flaws.Add(flaw);
            }

            return flaws;
        }

        public static List<Character_Flaws> ConvertToDao(List<CharacterFlawModel> flaws, int characterId)
        {
            var cf = new List<Character_Flaws>();
            foreach (CharacterFlawModel flaw in flaws)
            {
                cf.Add(new Character_Flaws()
                {
                    character_id = characterId,
                    flaw_id = flaw.FlawId,
                    created_date = DateTime.Today,
                    last_modified = DateTime.Today,
                    modified_by = ConverterHelper.USERSTAMP
                });
            }
            return cf;
        }
    }
}