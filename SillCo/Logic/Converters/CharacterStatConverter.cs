﻿using SillCo.Logic.Converters;
using SillCo.Models.Characters;
using SillCoDB;
using System;
using System.Collections.Generic;

namespace SillCo.Data.Converters
{
    public class CharacterStatConverter
    {
        public static CharacterStatModel ConvertToStatModel(List<Character_Attributes> attributes)
        {
            CharacterStatModel statModel = new CharacterStatModel();
            statModel.Agility = attributes.Find(x => x.attribute_id.Equals("Agi")).value.Value;
            statModel.Inspiration = attributes.Find(x => x.attribute_id.Equals("Ins")).value.Value;
            statModel.Build = attributes.Find(x => x.attribute_id.Equals("Bld")).value.Value;
            statModel.Creativity = attributes.Find(x => x.attribute_id.Equals("Cre")).value.Value;
            statModel.Fitness = attributes.Find(x => x.attribute_id.Equals("Fit")).value.Value;
            statModel.Influence = attributes.Find(x => x.attribute_id.Equals("Inf")).value.Value;
            statModel.Knowledge = attributes.Find(x => x.attribute_id.Equals("Kno")).value.Value;
            statModel.Perception = attributes.Find(x => x.attribute_id.Equals("Per")).value.Value;
            statModel.Psyche = attributes.Find(x => x.attribute_id.Equals("Psy")).value.Value;
            statModel.Willpower = attributes.Find(x => x.attribute_id.Equals("Wil")).value.Value;
            return statModel;
        }

        public static List<Character_Attributes> ConvertToDao(CharacterStatModel stats, int characterId)
        {
            List<Character_Attributes> caList = new List<Character_Attributes>();
            caList.Add(new Character_Attributes()
            {
                attribute_id = "Agi",
                character_id = characterId,
                value = stats.Agility,
                modified_by = ConverterHelper.USERSTAMP,
                created_date = DateTime.Today,
                last_modified = DateTime.Today
            });
            caList.Add(new Character_Attributes()
            {
                attribute_id = "Bld",
                character_id = characterId,
                value = stats.Build,
                modified_by = ConverterHelper.USERSTAMP,
                created_date = DateTime.Today,
                last_modified = DateTime.Today
            });
            caList.Add(new Character_Attributes()
            {
                attribute_id = "Cre",
                character_id = characterId,
                value = stats.Creativity,
                modified_by = ConverterHelper.USERSTAMP,
                created_date = DateTime.Today,
                last_modified = DateTime.Today
            });
            caList.Add(new Character_Attributes()
            {
                attribute_id = "Fit",
                character_id = characterId,
                value = stats.Fitness,
                modified_by = ConverterHelper.USERSTAMP,
                created_date = DateTime.Today,
                last_modified = DateTime.Today
            });
            caList.Add(new Character_Attributes()
            {
                attribute_id = "Inf",
                character_id = characterId,
                value = stats.Influence,
                modified_by = ConverterHelper.USERSTAMP,
                created_date = DateTime.Today,
                last_modified = DateTime.Today
            });
            caList.Add(new Character_Attributes()
            {
                attribute_id = "Ins",
                character_id = characterId,
                value = stats.Inspiration,
                modified_by = ConverterHelper.USERSTAMP,
                created_date = DateTime.Today,
                last_modified = DateTime.Today
            });
            caList.Add(new Character_Attributes()
            {
                attribute_id = "Kno",
                character_id = characterId,
                value = stats.Knowledge,
                modified_by = ConverterHelper.USERSTAMP,
                created_date = DateTime.Today,
                last_modified = DateTime.Today
            });
            caList.Add(new Character_Attributes()
            {
                attribute_id = "Per",
                character_id = characterId,
                value = stats.Perception,
                modified_by = ConverterHelper.USERSTAMP,
                created_date = DateTime.Today,
                last_modified = DateTime.Today
            });
            caList.Add(new Character_Attributes()
            {
                attribute_id = "Psy",
                character_id = characterId,
                value = stats.Psyche,
                modified_by = ConverterHelper.USERSTAMP,
                created_date = DateTime.Today,
                last_modified = DateTime.Today
            });
            caList.Add(new Character_Attributes()
            {
                attribute_id = "Wil",
                character_id = characterId,
                value = stats.Willpower,
                modified_by = ConverterHelper.USERSTAMP,
                created_date = DateTime.Today,
                last_modified = DateTime.Today
            });

            return caList;
        }
    }
}