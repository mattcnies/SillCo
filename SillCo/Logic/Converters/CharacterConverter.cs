﻿using SillCo.Logic.Converters;
using SillCo.Models.Characters;
using SillCoDB;
using System;
using System.Collections.Generic;

namespace SillCo.Data.Converters
{
    public class CharacterConverter
    {

        public static CharacterModel ConvertDaoToModel(Character dao)
        {
            CharacterModel c = new CharacterModel();
            c.CharacterId = dao.character_id;
            c.Name = dao.name;
            c.Profession = dao.profession;
            c.Nationality = dao.nationality;
            c.Gender = dao.gender;
            c.EyeColor = dao.eye_color;
            c.HairColor = dao.hair_color;
            c.Height = dao.height;
            c.Weight = dao.weight;
            c.Handedness = dao.handedness;
            c.Description = dao.desc;
            c.Age = dao.age != null ? dao.age.Value : 0;
            c.XP = dao.experience != null ? dao.experience.Value : 0;
            c.Year = dao.year != null ? dao.year.Value : 0;
            c.IsDead = dao.has_died != null ? dao.has_died.Value : false;
            return c;
        }

        public static Character ConvertToDao(CharacterModel character)
        {
            return ConvertToDao(character, false);
        }

        public static Character ConvertToDao(CharacterModel c, bool isInsert)
        {
            Character dao = new Character();
            dao.character_id = c.CharacterId;
            dao.name = c.Name;
            dao.profession = c.Profession;
            dao.nationality = c.Nationality;
            dao.gender = c.Gender;
            dao.age = c.Age;
            dao.eye_color = c.EyeColor;
            dao.hair_color = c.HairColor;
            dao.height = c.Height;
            dao.weight = c.Weight;
            dao.handedness = c.Handedness;
            dao.desc = c.Description;
            dao.experience = c.XP;
            dao.year = c.Year;
            dao.has_died = c.IsDead;
            dao.last_modified = DateTime.Today;
            dao.modified_by = ConverterHelper.USERSTAMP;

            if (isInsert)
            {
                dao.created_date = DateTime.Today;
            }

            return dao;
        }
    }
}