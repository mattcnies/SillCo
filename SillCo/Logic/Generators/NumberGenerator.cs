﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SillCo.Logic.Generators
{
    public class NumberGenerator
    {
        private Random random;

        public NumberGenerator(Random random)
        {
            this.random = random;
        }

        public int GenerateRandomNumber(int min, int max)
        {
            return random.Next(min, max + 1);
        }

        public int GetStat()
        {
            int g = GenerateRandomNumber(1, 100);
            if (g <= 5)
                return -1;
            if (g > 5 && g < 95)
                return 0;
            if (g >= 95)
                return 1;
            throw new Exception();
        }
    }
}