﻿using SillCo.Models.Characters;
using System.Collections.Generic;

namespace SillCo.Logic.Generators
{
    public class NPCGenerator
    {
        private List<SkillModel> skillList;
        private List<StatModel> statList;
        private NumberGenerator numberGenerator;

        public NPCGenerator(List<SkillModel> skillList, List<StatModel> statList, NumberGenerator numberGenerator)
        {
            this.skillList = skillList;
            this.statList = statList;
            this.numberGenerator = numberGenerator;
        }

        public List<CharacterModel> CreateNPCs(int num)
        {
            List<CharacterModel> characters = new List<CharacterModel>();
            
            for (int i = 0; i < num; i++)
            {
                characters.Add(createCharacter());
            }

            return new List<CharacterModel>();
        }

        CharacterModel createCharacter()
        {
            CharacterModel character = new CharacterModel();
            character.Name = "Create Random";
            character.Profession = "NPC";
            character.IsNpc = true;
            character.Stats = createStats();
            character.Skills = createSkills();
            return character;
        }

        CharacterStatModel createStats()
        {
            CharacterStatModel stat = new CharacterStatModel();
            stat.Agility = numberGenerator.GetStat();
            stat.Build = numberGenerator.GetStat();
            stat.Creativity = numberGenerator.GetStat();
            stat.Fitness = numberGenerator.GetStat();
            stat.Influence = numberGenerator.GetStat();
            stat.Inspiration = numberGenerator.GetStat();
            stat.Knowledge = numberGenerator.GetStat();
            stat.Perception = numberGenerator.GetStat();
            stat.Psyche = numberGenerator.GetStat();
            stat.Willpower = numberGenerator.GetStat();
            return stat;
        }

        List<CharacterSkillModel> createSkills()
        {
            List<CharacterSkillModel> skills = new List<CharacterSkillModel>();

            return skills;
        }
    }
}