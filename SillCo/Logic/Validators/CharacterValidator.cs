﻿using SillCo.Models.Characters;

namespace SillCo.Logic.Validators
{
    public class CharacterValidator
    {
        private CharacterModel character;

        public CharacterValidator(CharacterModel character)
        {
            this.character = character;
        }

        public CharacterModel Validate()
        {
            ValidateGender();
            return character;
        }

        void ValidateGender()
        {
            if (character.Gender.Length > 1)
            {
                character.Gender.Trim();
                
            }
        }

    }
}
