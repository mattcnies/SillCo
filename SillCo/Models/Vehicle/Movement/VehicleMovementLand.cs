﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SillCo.Models.Vehicle.Movement
{
    class VehicleMovementBasic : VehicleMovement
    {
        public int Speed { get; set; }

        public VehicleMovementBasic(int points, int maneuver, int deploy)
        {
            MovePoints = points;
            Maneuver = maneuver;
            DeploymentRange = deploy;

            CalcCombat();
            CalcSpeed();
        }

        private void CalcSpeed()
        {
            Speed = MovePoints * 6;
        }

    }
}
