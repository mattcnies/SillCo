﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SillCo.Models.Vehicle.Movement
{
    public enum MovementClass
    {
        Land,
        Naval,
        Air,
        Space
    }
}
