﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SillCo.Models.Vehicle.Movement
{
    public class VehicleMovementGround : VehicleMovement
    {
        public string Name
        {
            get
            {
                return "Ground:";
            }

        }

        public string Description
        {
            get
            {
                return "Air: this movement type is used by all flightcapable vehicles, including gravitic ones. " + 
                    "Each aircraft’s peculiar flight characteristics are provided by suitable Perks and Flaws(see Perks and Flaws)";
            }

        }

    }
}