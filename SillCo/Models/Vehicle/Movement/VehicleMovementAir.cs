﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SillCo.Models.Vehicle.Movement
{
    class VehicleMovementAir : VehicleMovement
    {
        public int Speed { get; private set; }
        public int StallSpeed { get; private set; }
        public bool VTOL { get; private set; } 

        public VehicleMovementAir(int points, bool vtol, int stall, int maneuver, int deploy)
        {
            MovePoints = points;
            Maneuver = maneuver;
            VTOL = vtol;
            StallSpeed = stall;
            DeploymentRange = deploy;

            CalcCombat();
            CalcSpeed();
        }

        private void CalcSpeed()
        {
            Speed = MovePoints * 30;
        }
    }
}
