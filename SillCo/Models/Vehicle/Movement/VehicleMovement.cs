﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SillCo.Models.Vehicle.Movement
{
    public class VehicleMovement
    {
        public int MovePoints { get; set; }
        public int Combat { get; set; }
        public int Maneuver { get; set; }
        public int DeploymentRange { get; set; }
        
        public void CalcCombat()
        {
            Combat = MovePoints % 2 != 0 ? 
                MovePoints / 2 + 1 : MovePoints / 2;
        }

    }
}