﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SillCo.Models.Vehicle.Movement
{
    class MovementTypeList
    {
        public static List<MovementType> GetList()
        {
            List<MovementType> typeList = new List<MovementType>();
            typeList.Add(new MovementType(MovementClass.Air, "Air", "This movement type is used by all flight capable vehicles, including gravitic ones."));
            typeList.Add(new MovementType(MovementClass.Land, "Ground", "Any wheeled or tracked vehicle."));
            typeList.Add(new MovementType(MovementClass.Land, "Hover", "Used by all vehicles which travel above but near the ground."));
            typeList.Add(new MovementType(MovementClass.Naval, "Naval", "Conventional water vessels and hydrofoils, or anything that can float"));
            typeList.Add(new MovementType(MovementClass.Land, "Rail", "Uses a rail or guide of some kind to move about."));
            typeList.Add(new MovementType(MovementClass.Space, "Space", "A vehicle equipped with reaction thrusters uses this movement system. Does not include atmospheric flight or reentry"));
            typeList.Add(new MovementType(MovementClass.Naval, "Submarine", "This movement type covers underwater craft of all sorts. Most submarines also have naval but it's not required."));
            typeList.Add(new MovementType(MovementClass.Land, "Walker", "This represents a multi-legged walking vehicle."));
            return typeList;
        }

    }
}
