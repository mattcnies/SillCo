﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SillCo.Models.Vehicle.Movement
{
    public class VehicleMovementHover : VehicleMovement
    {
        public string Name
        {
            get
            {
                return "Hover";
            }

        }

        public string Description
        {
            get
            {
                return "This movement type is used by all " +
                    "vehicles which travel above but near the " +
                    "ground, such as ground - effect hovercraft. " +
                    " Ground - effect systems only work within an " +
                    "atmosphere.";
            }

        }

    }
}