﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SillCo.Models.Vehicle.Movement
{
    class MovementType
    {
        public MovementClass MoveClass { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public MovementType(MovementClass clazz, string name, string desc)
        {
            MoveClass = clazz;
            Name = name;
            Description = desc;
        }
    }
}
