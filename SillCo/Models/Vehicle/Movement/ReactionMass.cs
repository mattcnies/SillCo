﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SillCoLib.Models.Vehicle.Movement
{
    class ReactionMass
    {
        public string RMType { get; set; }
        public decimal VolumePerWeight { get; set; }

    }
}
