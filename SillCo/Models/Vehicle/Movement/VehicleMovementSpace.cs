﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SillCo.Models.Vehicle.Movement
{
    class VehicleMovementSpace : VehicleMovement
    {
        public double Acceleration { get; private set; }

        public VehicleMovementSpace(int points, int maneuver, int deploy)
        {
            MovePoints = points;
            Maneuver = maneuver;
            DeploymentRange = deploy; // deployment is more complicated with reaction mass

            CalcCombat();
            CalcAcceleration();
        }

        private void CalcAcceleration()
        {
            Acceleration = (double)MovePoints * 0.1;
        }

    }
}
