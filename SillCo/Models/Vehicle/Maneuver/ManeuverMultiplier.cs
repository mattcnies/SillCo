﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SillCo.Models.Vehicle.Maneuver
{
    public class ManeuverMultiplier
    {
        public static Dictionary<int, double> GetManeuverMultiplier()
        {
            Dictionary<int, double> mm = new Dictionary<int, double>();
            mm.Add(3, 9);
            mm.Add(2, 3);
            mm.Add(1, 1.5);
            mm.Add(0, 1);
            mm.Add(-1, 0.67);
            mm.Add(-2, 0.5);
            mm.Add(-3, 0.4);
            mm.Add(-4, 0.33);
            mm.Add(-5, 0.28);
            mm.Add(-6, 0.25);
            mm.Add(-7, 0.22);
            mm.Add(-8, 0.2);
            mm.Add(-9, 0.18);
            mm.Add(-10, 0.16);
            return mm;
        }

    }
}