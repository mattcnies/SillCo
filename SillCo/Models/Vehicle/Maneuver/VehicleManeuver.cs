﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SillCo.Models.Vehicle.Maneuver
{
    class VehicleManeuver
    {
        public List<ManeuverType> ManeuverTypes { get; set; }

        public VehicleManeuver()
        {
            CreateManeuverList();
        }

        private void CreateManeuverList()
        {
            ManeuverTypes.Add(new ManeuverType(3, "", 9));
            ManeuverTypes.Add(new ManeuverType(2, "", 3));
            ManeuverTypes.Add(new ManeuverType(1, "Nimble Battlesuit, Dirt Bike", 1.5));
            ManeuverTypes.Add(new ManeuverType(0, "Nimble Mecha, Battlesuit, Motor Bike, Dirt Buggy", 1));
            ManeuverTypes.Add(new ManeuverType(-1, "Mecha, Car, Nimble Hovercraft", 0.67));
            ManeuverTypes.Add(new ManeuverType(-2, "Large Mecha, Large Car, Hovercraft, Fighter", 0.5));
            ManeuverTypes.Add(new ManeuverType(-3, "Truck, Tug Boat, Small Space Ship", 0.4));
            ManeuverTypes.Add(new ManeuverType(-4, "Patrol Boat, Large Truck, Medium Space Ship", 0.33));
            ManeuverTypes.Add(new ManeuverType(-5, "Large Hovercraft, Large Space Ship", 0.28));
            ManeuverTypes.Add(new ManeuverType(-6, "Large Navel Vessel or Ponderous", 0.25));
            ManeuverTypes.Add(new ManeuverType(-7, "", 0.22));
            ManeuverTypes.Add(new ManeuverType(-8, "Ponderous Navel Vessel", 0.2));
            ManeuverTypes.Add(new ManeuverType(-9, "Supertanker", 0.18));
            ManeuverTypes.Add(new ManeuverType(-10, "Space Station", 0.16));
        }

    }
}
