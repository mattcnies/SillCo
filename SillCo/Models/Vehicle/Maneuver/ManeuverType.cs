﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SillCo.Models.Vehicle.Maneuver
{
    class ManeuverType
    {
        public string Description { get; set; }
        public int Value { get; set; }
        public double Multiplier { get; set; }

        public ManeuverType(int value, string desc, double multi)
        {
            Description = desc;
            Value = value;
            Multiplier = multi;
        }
    }
}
