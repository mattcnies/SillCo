﻿using SillCo.Models.Vehicle.Movement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SillCo.Models.Vehicle
{
    public class Vehicle
    {
        //header
        public int Name { get; set; }
        public string Description { get; set; }
        public int Size { get; set; }
        public int Mass { get; set; }
        public int Threat { get; set; }
        public int Cost { get; set; }

        //defensive
        public int DefensiveThreatValue { get; set; }
        public List<VehicleMovement> MovementSystems { get; set; }
        public int Movement { get; set; }
        public int Maneuver { get; set; }
        public int ArmorRating { get; set; }

        //misc
        public int MiscThreatValue { get; set; }
        public int CrewSize { get; set; }
        public bool ComputerSystem { get; set; }
        public int Passengers { get; set; }
        public int DeploymentRange { get; set; }
        public List<string> Perks { get; set; }
        public List<string> Flaws { get; set; }

        //offensive
        public int OffensiveThreatValue { get; set; }
        public List<VehicleOffenseiveSystem> OffensiveSystems { get; set; }

        public void SetTargetSize(int size)
        {
            Size = size;
            CalcMass();
        }

        public void SetArmorRating(int armor)
        {
            ArmorRating = armor;
        }

        public void SetCrewAndPassengers(int crew, int passengers, bool hasComputer)
        {
            CrewSize = crew;
            Passengers = passengers;
            ComputerSystem = hasComputer;
        }

        public void SetMovementSystems(List<VehicleMovement> movement)
        {
            MovementSystems = movement;
        }

        public void CalcMass()
        {
            //Maximum Mass = ((Size + 0.5) x 3)3
            double calc = ((double)Size + 0.5);
            calc = calc * 3;
            calc = calc * calc * calc;
            Mass = (int)Math.Ceiling(calc);
        }

        public void CalcDefensiveThreat()
        {
            DefensiveThreatValue = 0;
            DefensiveThreatValue += ArmorRating * ArmorRating;
            foreach (VehicleMovement movement in MovementSystems)
            {
                DefensiveThreatValue += movement.MovePoints * movement.MovePoints;
                DefensiveThreatValue += (int)Math.Ceiling((decimal)(DefensiveThreatValue * movement.Maneuver));
            }
        }

        public void CalcOffensiveThreat()
        {

        }

        public void CalcMiscThreat()
        {
            MiscThreatValue = 0;
            // actions * actions * actions
            MiscThreatValue += (DeploymentRange / 50) * (DeploymentRange / 50);
            // burn points / 100 * burn points / 100
            // perk point total * perk point total
            // -flaw point total * flaw point total

            if (MiscThreatValue < 0)
            {
                MiscThreatValue = 0;
            }
        }

        public void CalcThreat()
        {
            decimal d = (OffensiveThreatValue + DefensiveThreatValue + MiscThreatValue) / 3;
            Threat = (int)Math.Ceiling(d);
        }

        public void CalcCost()
        {
            Cost = Threat * 1000;
        }
    }
}