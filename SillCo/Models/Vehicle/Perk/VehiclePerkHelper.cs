﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SillCo.Models.Vehicle.Perk
{
    class VehiclePerkHelper
    {
        public static List<VehiclePerkListItem> GetVehiclePerkList()
        {
            List<VehiclePerkListItem> perks = new List<VehiclePerkListItem>();
            perks.AddRange(CreateAccessoryList());
            perks.AddRange(CreateArmsList());
            return perks;
        }

        static List<VehiclePerkListItem> CreateAccessoryList()
        {
            List<VehiclePerkListItem> perks = new List<VehiclePerkListItem>();
            perks.Add(new VehiclePerkListItem(
                VehiclePerkType.Accessories, "Acceleration Protection", "1",
                ""));
            perks.Add(new VehiclePerkListItem(
                VehiclePerkType.Accessories, "Airlift Winch", "Rating",
                ""));
            perks.Add(new VehiclePerkListItem(
                VehiclePerkType.Accessories, "Autopilot", "5",
                ""));
            perks.Add(new VehiclePerkListItem(
                VehiclePerkType.Accessories, "Catauplt", "Rating",
                ""));
            perks.Add(new VehiclePerkListItem(
                VehiclePerkType.Accessories, "Emergency Medical", "1 x number of crew actions\n0.1 x number of passengers",
                ""));
            perks.Add(new VehiclePerkListItem(
                VehiclePerkType.Accessories, "Escape System", "total number of actions due to crew (ejection seats)\ntotal number of actions due to crew x 2 (escapte pods)",
                ""));
            perks.Add(new VehiclePerkListItem(
                VehiclePerkType.Accessories, "Gun Ports", "1 (gun pors for 1/3 of the crew and passengers)\n2 (gun ports for half the crew and passengers)",
                ""));
            perks.Add(new VehiclePerkListItem(
                VehiclePerkType.Accessories, "Large Doors", "1",
                ""));
            perks.Add(new VehiclePerkListItem(
                VehiclePerkType.Accessories, "Life Support", "1 x number of crew actions (limited life support)\n3 x number of crew actions (full life support)",
                ""));
            perks.Add(new VehiclePerkListItem(
                VehiclePerkType.Accessories, "Loudspeakers", "1",
                ""));
            perks.Add(new VehiclePerkListItem(
                VehiclePerkType.Accessories, "Mining Equipment", "5 (Light Duty)\n20 (Heavy Duty)",
                ""));
            perks.Add(new VehiclePerkListItem(
                VehiclePerkType.Accessories, "Pintle Mount", "5",
                ""));
            perks.Add(new VehiclePerkListItem(
                VehiclePerkType.Accessories, "Power Booster", "Rating x Rating",
                ""));
            perks.Add(new VehiclePerkListItem(
                VehiclePerkType.Accessories, "Ram Plate", "4",
                ""));
            perks.Add(new VehiclePerkListItem(
                VehiclePerkType.Accessories, "Refueling Equipment", "2 per refueling boom\n1 for intake and receiving equipment",
                ""));
            perks.Add(new VehiclePerkListItem(
                VehiclePerkType.Accessories, "Seachlight", "1 per 200m range (fixed)\n2 per 200m range (swivel)",
                ""));
            return perks;
        }

        static List<VehiclePerkListItem> CreateArmsList()
        {
            List<VehiclePerkListItem> perks = new List<VehiclePerkListItem>();
            perks.Add(new VehiclePerkListItem(
                VehiclePerkType.Arms, "Battle Arm", "0.2 x Rating for each arm",
                ""));
            perks.Add(new VehiclePerkListItem(
                VehiclePerkType.Arms, "Manipulator Arm", "0.5 x Rating for each arm",
                ""));
            perks.Add(new VehiclePerkListItem(
                VehiclePerkType.Arms, "Tool Arm", "0.3 x Rating for each arm",
                ""));
            return perks;
        }
    }
}
