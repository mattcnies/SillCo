﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SillCo.Models.Vehicle.Perk
{
    class VehiclePerk
    {
        public VehiclePerkType PerkType { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Cost { get; set; }

        public VehiclePerk(VehiclePerkType accessories, string v1, string v2, double v3)
        {
            PerkType = accessories;
            Name = v1;
            Description = v2;
            Cost = v3;
        }
    }
}
