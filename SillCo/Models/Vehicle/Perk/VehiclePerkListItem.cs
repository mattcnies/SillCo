﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SillCo.Models.Vehicle.Perk
{
    class VehiclePerkListItem
    {
        public VehiclePerkType PerkType { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Cost { get; set; }

        public VehiclePerkListItem(VehiclePerkType accessories, string name, string cost, string desc)
        {
            PerkType = accessories;
            Name = name;
            Description = desc;
            Cost = cost;
        }

    }
}
