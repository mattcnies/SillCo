﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SillCo.Models.Vehicle.Perk
{
    public enum VehiclePerkType
    {
        Accessories,
        Arms,
        ArmorQualities,
        Communications,
        Features,
        HostileEnvProt,
        InfoWarfare,
        Reinforced,
        Sensors,
        Teleporter,
        Thought,
        Dimensional
    }
}
