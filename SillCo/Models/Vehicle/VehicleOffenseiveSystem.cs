﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SillCo.Models.Vehicle
{
    public class VehicleOffenseiveSystem
    {
        public int FiringArc { get; set; }
        public int Accuracy { get; set; }
        public int DamageMultiplier { get; set; }
        public int BaseRange { get; set; }
        public int RateOfFire { get; set; }
        public List<string> Characteristics { get; set; }
    }
}