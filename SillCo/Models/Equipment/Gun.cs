﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SillCo.Models.Equipment
{
    public class Gun
    {
        public string Name { get; set; }
        public int Ammo { get; set; }
        public int Magazine { get; set; }

        public Gun(string name, int ammo, int magazines)
        {
            Name = name;
            Ammo = ammo;
            Magazine = magazines;
        }

        public void Shoot()
        {
            if (Ammo > 0)
            {
                Ammo--;
            }
        }

        //added Jan 27, 2017
        public void Shoot(int rof)
        {
            if (Ammo > 0)
            {
                if(rof==0) {
                    Ammo--;
                } else {
                    Ammo -= (rof * 5);
                }

                Ammo--;
            }
        }

        public void Reload()
        {
            if (Magazine > 0)
            {
                Magazine--;
            }
        }
    }
}