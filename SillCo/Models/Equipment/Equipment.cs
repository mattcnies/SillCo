﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SillCo.Models.Equipment
{
    public class Equipment
    {
        public string Name { get; set; }
        public string Weight { get; set; }
        public string Armor { get; set; }
        public string Complexity { get; set; }
        public string Note { get; set; }
        public string Cost { get; set; }
    }
}