﻿using SillCo.Models.Characters;
using SillCoDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SillCo.Models.Player
{
    public class Player
    {
        public string Name { get; set; }
        public Character Character { get; set; }
    }
}