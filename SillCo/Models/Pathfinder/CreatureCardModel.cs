﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SillCo.Models.Pathfinder
{
    public class CreatureCardModel
    {
        public string Name { get; set; }
        public string Base { get; set; }
        public string Defense { get; set; }
        public string Offenes { get; set; }
        public string Statistics { get; set; }
        public string Moral { get; set; }

        public int Order { get; set; }
    }
}