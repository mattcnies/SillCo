﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SillCo.Models.Characters
{
    public class CharacterLanguageModel
    {
        public int LanguageId { get; set; }
        public string Name { get; set; }
    }
}