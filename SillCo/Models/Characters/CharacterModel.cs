﻿using System;
using System.Collections.Generic;

namespace SillCo.Models.Characters
{
    public class CharacterModel
    {
        public int CharacterId { get; set; }

        public string Name { get; set; }
        public string Profession { get; set; }
        public string Nationality { get; set; }
        public string Gender { get; set; }
        public string EyeColor { get; set; }
        public string HairColor { get; set; }
        public string Height { get; set; }
        public string Weight { get; set; }
        public string Handedness { get; set; }
        public string Description { get; set; }
        public int Age { get; set; }
        public int XP { get; set; }
        public int Year { get; set; }
        public bool IsDead { get; set; }
        public bool IsNpc { get; set; }
        public int XPSpent { get; private set; }
        //public int Threat { get; set; }

        public CharacterStatModel Stats { get; set; }
        public List<CharacterSkillModel> Skills { get; set; }
        public List<CharacterLanguageModel> Languages { get; set; }
        public List<CharacterInjuryModel> Injuries { get; set; }
        public List<CharacterPerkModel> Perks { get; set; }
        public List<CharacterFlawModel> Flaws { get; set; }

        public CharacterModel()
        {
            Stats = new CharacterStatModel();
            Skills = new List<CharacterSkillModel>();
            Languages = new List<CharacterLanguageModel>();
            Injuries = new List<CharacterInjuryModel>();
            Perks = new List<CharacterPerkModel>();
            Flaws = new List<CharacterFlawModel>();
        }

        public void CalcuateXPSpent()
        {
            double xptotal = 0d;
            Skills.ForEach(s =>
            {
                for (int i = 1; i <= s.Skill; i++)
                {
                    if (i != 1)
                    {
                        xptotal += Math.Pow(i, 2d);
                    }
                }

                for (int i = 1; i <= s.Complexity; i++)
                {
                    xptotal += Math.Pow(i, 2d);
                }

                if (!String.IsNullOrEmpty(s.Specialization))
                {
                    xptotal += 10d;
                }
            });
            XPSpent = Convert.ToInt32(xptotal);
        }
    }
}