﻿using System;

namespace SillCo.Models.Characters
{
    public class CharacterStatModel
    {
        private int unarmedSkill = 0;
        private int meleeSkill = 0;
        private int stamBuff = 0;

        public int Agility { get; set; }
        public int Inspiration { get; set; }
        public int Build { get; set; }
        public int Creativity { get; set; }
        public int Fitness { get; set; }
        public int Influence { get; set; }
        public int Knowledge { get; set; }
        public int Perception { get; set; }
        public int Psyche { get; set; }
        public int Willpower { get; set; }

        public int Strength
        {
            get
            {
                return (int)Math.Round(((decimal)Build + (decimal)Fitness) / 2);
            }
        }

        public int Health
        {
            get
            {
                return (int)Math.Round(((decimal)Fitness + (decimal)Build + (decimal)Willpower) / 3);
            }
        }

        public int Stamina
        {
            get
            {
                return (int)(5 * (Build + Health)) + (25 + stamBuff);
            }
        }

        public int UnarmedDamage
        {
            get
            {
                int the_damage = 3 + Strength + Build + unarmedSkill;
                if (the_damage < 1)
                {
                    the_damage = 1;
                }
                return the_damage;  
            }
        }

        public int ArmedDamage
        {
            get
            {
                int the_damage = 3 + Strength + Build + meleeSkill;
                if (the_damage < 1)
                {
                    the_damage = 1;
                }
                return the_damage;  
            }
        }

        public int FleshWound
        {
            get
            {
                return (int)Math.Round((decimal)Stamina / 2);
            }
        }

        public int DeepWound
        {
            get
            {
                return Stamina;
            }
        }

        public int InstantDeath
        {
            get
            {
                return Stamina * 2;
            }
        }

        public int SystemShock
        {
            get
            {
                return 5 + Health;
            }
        }

        public void ApplyBuffs(int? unarmed, int? melee)
        {
            if (unarmed != null)
                this.unarmedSkill = unarmed.Value;

            if (melee != null)
                this.meleeSkill = melee.Value;
        }

    } 
}