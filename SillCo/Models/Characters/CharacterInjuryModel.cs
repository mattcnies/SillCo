﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SillCo.Models.Characters
{
    public class CharacterInjuryModel
    {
        public int CharacterInjuryId { get; set; }
        public int Modifier { get; set; }
        public string InjuryType { get; set; }
        public bool IsHealed { get; set; }
    }
}