﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SillCo.Models.Characters.Creation
{
    public class CharacterCreationRulesModel
    {
        public int StartingXP { get; set; }
        public int StartingThreat { get; set; }
        public int StartingCP { get; set; }
    }
}