﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SillCo.Models.Characters.Creation
{
    public class CharacterCreationModel
    {
        public CharacterModel Character { get; set; }
        public CharacterCreationRulesModel Rules { get; set; }
    }
}