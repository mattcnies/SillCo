﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SillCo.Models.Characters
{
    public class CharacterSkillModel
    {
        public int ID { get; set; }
        public int AttributeId { get; set; }

        public string Name { get; set; }
        public int Skill { get; set; }
        public int Complexity { get; set; }
        public string Specialization { get; set; }
        public string Attribute { get; set; }

    }
}