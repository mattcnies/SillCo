﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SillCo.Models.Characters
{
    public class SkillModel
    {
        public int SkillId { get; set; }
        public int? AttributeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}