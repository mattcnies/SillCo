﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SillCo.Models.Characters
{
    public class CharacterFlawModel
    {
        public int FlawId { get; set; }
        public string Name { get; set; }
        public string Desc { get; set; }
        public int Modifier { get; set; }
    }
}