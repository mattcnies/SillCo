﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SillCo.Models.Characters
{
    public class StatModel
    {
        public int AttributeId { get; set; }
        public string Name { get; set; }
        public string Abbrev { get; set; }
    }
}