﻿using LinqToExcel;
using SillCo.Models.Equipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SillCo.Data
{
    /* 
     * this is all going to be redone for the database model
     */
    public class FileHelper
    {
        public static readonly string PLAYER_FILE = @"E:\projects\web\sillheutte\SillCoData\Players.json";
        public static readonly string EQUIP_FILE = @"E:\projects\web\sillheutte\SillCoData\Equipment.xls";

        public static List<Equipment> EquipmentExcel(string tab)
        {
            var excel = new ExcelQueryFactory(EQUIP_FILE);
            excel.AddMapping<Equipment>(x => x.Name, "EQUIPMENT");
            excel.AddMapping<Equipment>(x => x.Weight, "Weight");
            excel.AddMapping<Equipment>(x => x.Armor, "ARMOR");
            excel.AddMapping<Equipment>(x => x.Complexity, "CLPX");
            excel.AddMapping<Equipment>(x => x.Note, "NOTES");
            excel.AddMapping<Equipment>(x => x.Cost, "COST");
            
            var query = from x in excel.Worksheet<Equipment>(tab)
                        select x;

            return query.ToList<Equipment>();
        }

    }
}