﻿using SillCo.Models.Equipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SillCo.Data.List
{
    public class EquipmentList
    {
        public List<Equipment> Tools { get; private set; }
        public List<Equipment> Apparel { get; private set; }
        public List<Equipment> Armor { get; private set; }
        public List<Equipment> Medical { get; private set; }
        public List<Equipment> Electrical { get; private set; }
        public List<Equipment> Survival { get; private set; }
        public List<Equipment> Misc { get; private set; }

        public EquipmentList()
        {
            Tools = FileHelper.EquipmentExcel("Tools");
            Apparel = FileHelper.EquipmentExcel("Apparel");
            Armor = FileHelper.EquipmentExcel("Armor");
            Medical = FileHelper.EquipmentExcel("Medical");
            Electrical = FileHelper.EquipmentExcel("Electrical");
            Survival = FileHelper.EquipmentExcel("Survival");
            Misc = FileHelper.EquipmentExcel("Misc");
        }

    }
}