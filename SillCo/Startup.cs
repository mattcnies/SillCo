﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SillCo.Startup))]
namespace SillCo
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
