﻿using SillCo.Models.Equipment;
using System.Web.Mvc;

namespace SillCo.Controllers
{
    public class HomeController : Controller
    {


        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }
        
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        // GET: /Home/Character
        public ActionResult Character()
        {
            ViewBag.Message = "Your Character page.";
            Gun gun = new Gun("Light Pistol", 10, 2);
            return View(gun);
        }

        public ActionResult Jobs()
        {
            ViewBag.Title = "Jobs";
            ViewBag.Message = "Do some cool space shit with USN Marines";

            return View();   
        }

        // POST: /Home/Character
        [HttpPost]
        public ActionResult Character(Gun gun)
        {
            gun.Shoot();

            return View(gun);
        }

    }
}