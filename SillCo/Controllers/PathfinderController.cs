﻿using SillCo.Models.Pathfinder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SillCo.Controllers
{
    public class PathfinderController : Controller
    {
        // GET: Pathfinder
        public ActionResult Index()
        {
            List<CreatureCardModel> models = new List<CreatureCardModel>();
            CreatureCardModel model = new CreatureCardModel();
            model.Name = "BAT SWARM";
            model.Base = "Init +2; Senses blindsense 20 ft., low-light vision; Perception +15";
            model.Defense = "AC 16, touch 16, flat-footed 14 (+2 Dex, +4 size) hp 13 (3d8) Fort +3, Ref +7, Will +3 Defensive Abilities swarm traits; Immune weapon damage";
            model.Offenes = "Speed 5 ft., fly 40 ft. (good) Melee swarm (1d6) Space 10 ft.; Reach 0 ft. Special Attacks distraction (DC 11), wounding";
            model.Statistics = "Str 3, Dex 15, Con 11, Int 2, Wis 14, Cha 4 Base Atk +2; CMB —; CMD — Feats Lightning Reflexes, Skill Focus (Perception) Skills Fly +12, Perception +15; Racial Modifiers +4 Perception when using blindsense SQ swarm traits";
            model.Moral = "Fight to the death";
            model.Order = 1;
            models.Add(model);
            model = new CreatureCardModel();
            model.Name = "BANDIT";
            model.Base = "Init +2; Senses Perception –1";
            model.Defense = "AC 17, touch 13, flat-footed 14 (+3 armor, +2 Dex, +1 dodge, +1 shield) hp 11 (2d10) Fort +3, Ref +2, Will –1";
            model.Offenes = "Speed 30 ft. Melee rapier +3 (1d6+1/18–20) or sap +3 (1d6+1 nonlethal) Ranged composite longbow +4 (1d8+1/×3)";
            model.Statistics = "Str 13, Dex 14, Con 11, Int 10, Wis 8, Cha 9 Base Atk +2; CMB +3; CMD 16 Feats Dodge, Point Blank Shot Skills Climb +4, Handle Animal +3, Intimidate +3, Ride +5, Stealth +2 Languages Common";
            model.Moral = "Fight to the death";
            model.Order = 2;
            models.Add(model);
            return View(models);
        }

        // GET: Pathfinder/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Pathfinder/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Pathfinder/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Pathfinder/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Pathfinder/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Pathfinder/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Pathfinder/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
