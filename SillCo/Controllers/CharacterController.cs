﻿using SillCo.Data.Converters;
using SillCo.Models.Characters;
using System.Web.Mvc;
using System.Linq;
using System.Collections.Generic;
using SillCoDB;
using System;

namespace SillCo.Controllers
{
    public class CharacterController : Controller
    {

        // GET: Character
        public ActionResult Index()
        {
            ViewBag.Title = "Character Page";
            List<CharacterModel> characters = new List<CharacterModel>();

            using (var db = new SillCoContext())
            {
                var list = from b in db.Characters
                           select b;
                foreach (Character character in list)
                {
                    characters.Add(CharacterConverter.ConvertDaoToModel(character));
                }
            }

            return View(characters);
        }

        // GET: Character/Details/5
        public ActionResult Details(int id)
        {
            CharacterModel character;
            using (var db = new SillCoContext())
            {
                character = CharacterConverter.ConvertDaoToModel(db.Characters.Find(id));

                // Character Stats
                var stats = (from c in db.Character_Attributes
                             where c.character_id == id
                             select c).ToList();
                character.Stats = CharacterStatConverter.ConvertToStatModel(stats);

                // Character skills
                var skills = (from c in db.Character_Skills
                              where c.character_id == id
                              select c).ToList();
                character.Skills = CharacterSkillConverter.ConvertToModel(skills);

                int? unarmed = skills.Find(s => s.skill_id == 7)?.skill_points;
                int? melee = skills.Find(s => s.skill_id == 19)?.skill_points;
                character.Stats.ApplyBuffs(unarmed, melee);

                // Character flaws
                var flaws = (from c in db.Character_Flaws
                             where c.character_id == id
                             select c).ToList();
                character.Flaws = CharacterFlawConverter.ConvertToModel(flaws);

                // Character perks
                var perks = (from c in db.Character_Perks
                             where c.character_id == id
                             select c).ToList();
                character.Perks = CharacterPerkConverter.ConvertToModel(perks);

                // Character languages
                var langs = (from c in db.Character_Languages
                             where c.character_id == id
                             select c).ToList();
                character.Languages = CharacterLanguageConverter.ConvertToLanguageModel(langs);

                // Character Injuries
                CharacterInjuryConverter injuryConverter = new CharacterInjuryConverter();
                var injuries = (from c in db.Character_Injuries
                                where c.character_id == id &&
                                      !c.is_healed.Value
                                select c).ToList();
                character.Injuries = injuryConverter.ConvertToInjuryModel(injuries);
            }
            character.CalcuateXPSpent();
            return View(character);
        }

        // GET: Character/Create
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Character/Create
        [HttpPost]
        public ActionResult Create(CharacterModel character)
        {
            character.Name = "Quintus";
            character.Profession = "Contracted Demolitions";
            character.Nationality = "Spacer";
            character.Gender = "M";
            character.EyeColor = "Brown";
            character.HairColor = "Brown";
            character.Height = "Average";
            character.Weight = "Average";
            character.Handedness = "Right";
            character.Description = "A man that has a mysterious past. He connects better with Reticulan's than humans.";
            character.Age = 35;
            character.XP = 219;
            character.Year = 9999;
            character.IsDead = false;
            character.IsNpc = false;

            character.Languages = new List<CharacterLanguageModel>
            {
                new CharacterLanguageModel()
                {
                    LanguageId = 1,
                    Name = "English"
                },
                new CharacterLanguageModel()
                {
                    LanguageId = 4,
                    Name = "Reticulan"
                }
            };

            character.Stats = new CharacterStatModel()
            {
                Agility = 1,
                Inspiration = 0,
                Build = 0,
                Creativity = 1,
                Fitness = 0,
                Influence = 1,
                Knowledge = 0,
                Perception = 1,
                Psyche = 2,
                Willpower = 0
            };

            character.Skills = new List<CharacterSkillModel>()
            {
                new CharacterSkillModel() { ID = 4, Skill = 1, Complexity = 1 },  //slight of hand
                new CharacterSkillModel() { ID = 5, Skill = 2, Complexity = 2 },  //Small arms
                new CharacterSkillModel() { ID = 6, Skill = 1, Complexity = 1 },  //stealth
                new CharacterSkillModel() { ID = 7, Skill = 4, Complexity = 3 },  //unarmed
                new CharacterSkillModel() { ID = 8, Skill = 1, Complexity = 1 },  //zero g
                new CharacterSkillModel() { ID = 10, Skill = 2, Complexity = 2 }, //combat sense
                new CharacterSkillModel() { ID = 11, Skill = 4, Complexity = 4 }, //defense
                new CharacterSkillModel() { ID = 14, Skill = 1, Complexity = 1 }, //notice
                new CharacterSkillModel() { ID = 17, Skill = 1, Complexity = 1 }, //g handle
                new CharacterSkillModel() { ID = 23, Skill = 2, Complexity = 2 }, //Demolitions
                new CharacterSkillModel() { ID = 24, Skill = 1, Complexity = 1 }, //Disguise
                new CharacterSkillModel() { ID = 30, Skill = 1, Complexity = 1 }, //Tactics
                new CharacterSkillModel() { ID = 34, Skill = 1, Complexity = 1 }, //Communications
                new CharacterSkillModel() { ID = 50, Skill = 2, Complexity = 2 }, //Bluff
                new CharacterSkillModel() { ID = 54, Skill = 2, Complexity = 1 }, //Leadership
                new CharacterSkillModel() { ID = 55, Skill = 2, Complexity = 1 }, //Human Perception
                new CharacterSkillModel() { ID = 56, Skill = 2, Complexity = 1 }, //Psychic Defense
                new CharacterSkillModel() { ID = 57, Skill = 3, Complexity = 3,   //Pyrokinetics
                    Specialization = "Fire Boxer" }
            };

            character.Perks = new List<CharacterPerkModel>()
            {
                new CharacterPerkModel(){ PerkId = 30 },
                new CharacterPerkModel(){ PerkId = 70}
            };

            character.Flaws = new List<CharacterFlawModel>()
            {
                new CharacterFlawModel(){ FlawId = 81},
                new CharacterFlawModel(){ FlawId = 32},
                new CharacterFlawModel(){ FlawId = 59},
                new CharacterFlawModel(){ FlawId = 65}
            };

            try
            {
                using (var db = new SillCoContext())
                {
                    Character c = CharacterConverter.ConvertToDao(character, true);
                    c = db.Characters.Add(c);
                    db.Character_Attributes.AddRange(CharacterStatConverter.ConvertToDao(character.Stats, c.character_id));
                    db.Character_Skills.AddRange(CharacterSkillConverter.ConvertToDao(character.Skills, c.character_id));
                    db.Character_Languages.AddRange(CharacterLanguageConverter.ConvertToDao(character.Languages, c.character_id));
                    db.Character_Flaws.AddRange(CharacterFlawConverter.ConvertToDao(character.Flaws, c.character_id));
                    db.Character_Perks.AddRange(CharacterPerkConverter.ConvertToDao(character.Perks, c.character_id));
                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return View();
            }
        }

        // GET: Character/Edit/5
        public ActionResult Edit(int id)
        {
            CharacterModel character;
            using (var db = new SillCoContext())
            {
                character = CharacterConverter.ConvertDaoToModel(db.Characters.Find(id));
                var query = from c in db.Character_Skills
                            where c.character_id == id
                            select c;

                character.Skills = CharacterSkillConverter.ConvertToModel(query.ToList());
            }

            return View(character);
        }

        // POST: Character/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Character/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Character/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
