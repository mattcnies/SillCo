//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SillCoDB
{
    using System;
    using System.Collections.Generic;
    
    public partial class Flaw
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Flaw()
        {
            this.Character_Flaws = new HashSet<Character_Flaws>();
        }
    
        public int flaw_id { get; set; }
        public string name { get; set; }
        public string desc { get; set; }
        public Nullable<int> value { get; set; }
        public string modified_by { get; set; }
        public Nullable<System.DateTime> created_date { get; set; }
        public Nullable<System.DateTime> last_modified { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Character_Flaws> Character_Flaws { get; set; }
    }
}
