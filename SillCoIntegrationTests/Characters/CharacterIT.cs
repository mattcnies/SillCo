﻿using SillCo.Models.Characters;
using System;

namespace SillCoIntegrationTests.Characters
{
    class CharacterIT
    {

        public void TestGetCharacterByID()
        {
            CharacterModel character = new CharacterModel();
            Console.WriteLine(character.Name);
        }

        public void TestInsertCharacter()
        {
            CharacterModel c = new CharacterModel();
            c.Name = "Max Test";
            c.Profession = "Test profession";
            c.Nationality = "test nationality";
            c.Gender = "M";
            c.EyeColor = "black";
            c.HairColor = "black";
            c.Height = "average";
            c.Weight = "average";
            c.Handedness = "both";
            c.Description = "test character that is made up";
            c.Age = 99;
            c.XP = 70;
            c.Year = 9999;
            Console.WriteLine(c.CharacterId);
        }
    }
}
