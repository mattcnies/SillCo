﻿using SillCo.Models.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SillCoIntegrationTests.Characters
{
    class CharacterStatsIt
    {
        public void testInsert()
        {
            CharacterModel chara = new CharacterModel();
            chara.CharacterId = 2;
            CharacterStatModel stats = new CharacterStatModel();
            stats.Agility = 1;
            stats.Knowledge = 1;
            stats.Perception = 1;
            stats.Build = 1;
            stats.Creativity = 1;
            stats.Fitness = 1;
            stats.Influence = 1;
            stats.Psyche = 1;
            stats.Willpower = 1;
            chara.Stats = stats;
        }
    }
}
