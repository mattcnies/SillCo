SELECT character_injury_id
     , character_id
	 , injury_type
	 , modifier
	 , is_healed
	 , modified_by
	 , created_date
	 , last_modified
  FROM Character_Injuries
 WHERE character_id = @character_id