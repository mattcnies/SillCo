SELECT character_id
     , skill_id
	 , skill_points
	 , complexity
	 , specialization
	 , modified_by
	 , created_date
	 , last_modified
  FROM Character_Skills
 WHERE character_id = @character_id