SELECT character_id
     , language_id
	 , modified_by
	 , created_date
	 , last_modified
  FROM Character_Languages
 WHERE character_id = @character_id