/*
alter table [dbo].[Characters]
  drop CONSTRAINT FK_Characters_player_id;

truncate table [dbo].[Character_Attributes];
truncate table [dbo].[Character_Flaws];
truncate table [dbo].[Character_Injuries];
truncate table [dbo].[Character_Languages];
truncate table [dbo].[Character_Perks];
truncate table [dbo].[Character_Skills];
truncate table [dbo].[Characters];
truncate table [dbo].[Players];

alter table [dbo].[Characters]
  add constraint FK_Characters_player_id
		foreign key (player_id) references Players(player_id)
  			on delete cascade 
			on update cascade;
*/

insert into Players(name, login, password) values 
	('Matt Nies', 'mnies', null);

insert into Characters (player_id, [name], profession, nationality, gender, eye_color, hair_color, height, [weight], handedness, [desc], age, experience, [year], has_died, is_npc) values 
	(1, 'Max Nice', 'Combat Medic', 'Spacer', 'M',  'blue', 'brown', 'average', 'average', 'right', 'Retired USN Marine', 27, 110, 1, 1, 0);

insert into Character_Skills (character_id, skill_id, skill_points, complexity, specialization) values 
	(1, 7, 1, 1, null),  --Unarmed/Dodge
	(1, 5, 3, 2, 'SMG'), --Small Arms
	(1, 6, 1, 1, null),  --Stealth
	(1, 8, 1, 1, null),  --Zero G. Movement
	(1, 13, 1, 1, null), --Investigation
	(1, 10, 2, 2, null), --Combat Sense
	(1, 14, 2, 2, null), --Notice
	(1, 11, 3, 2, null), --Defense
	(1, 17, 2, 1, null), --G. Handling
	(1, 32, 1, 1, null), --Tinker
	(1, 31, 1, 1, null), --Teaching
	(1, 43, 1, 1, null), --Mechanics
	(1, 42, 1, 1, null), --Mechanical Design
	(1, 44, 3, 2, null), --Medicine
	(1, 41, 1, 1, null), --Life Sciences
	(1, 38, 3, 3, null); --First Aid

insert into Character_Attributes (character_id, attribute_id, [value]) values 
	(1, 'Agi', 1),
	(1, 'Bld', 0),
	(1, 'Cre', 1),
	(1, 'Fit', 0),
	(1, 'Inf', 0),
	(1, 'Ins', 0),
	(1, 'Kno', 2),
	(1, 'Per', 1),
	(1, 'Psy', 0),
	(1, 'Wil', 1);

insert into Character_Languages(character_id, language_id) values
	(1, 1),
	(1, 2),
	(1, 3);

insert into Character_Injuries(character_id, injury_type, modifier, is_healed) values
	(1, 'Flesh', -1, 0),
	(1, 'Deep', -2, 0);

insert into Character_Perks(character_id, perk_id) values 
	(1, 53),
	(1, 54);

insert into Character_Flaws(character_id, flaw_id) values 
	(1, 20);