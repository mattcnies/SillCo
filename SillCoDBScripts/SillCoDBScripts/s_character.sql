select character_id
	 , player_id
	 , [name]
	 , profession
	 , nationality
	 , gender
	 , eye_color
	 , hair_color
	 , height
	 , [weight]
	 , handedness
	 , [desc]
	 , age
	 , experience
	 , [year]
	 , has_died
	 , modified_by
	 , created_date
	 , last_modified
  from Characters
 where character_id = @character_id
;