SELECT character_id
	 , perk_id
	 , modified_by
	 , created_date
	 , last_modified
  FROM Character_Perks
 WHERE character_id = @character_id