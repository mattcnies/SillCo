MERGE Characters AS T
USING (SELECT 10 as character_id) as S ON T.character_id = S.character_id
WHEN MATCHED THEN 
	UPDATE SET T.name = 'Matt Nies', T.last_modified = GETDATE()
WHEN NOT MATCHED THEN
	INSERT (player_id, [name], profession, nationality, gender, eye_color, hair_color, height, [weight], handedness, [desc], age, experience, [year])
	VALUES (1, 'Max Nice', 'Combat Medic', 'Spacer', 'M',  'blue', 'brown', 'average', 'average', 'right', 'Retired USN Marine', 27, 56, 1)
;

select * from Characters;