SELECT flaw_id
     , [name]
	 , [desc]
	 , [value]
	 , modified_by
	 , created_date
	 , last_modified
  FROM Flaws