SELECT character_id
	 , flaw_id
	 , modified_by
	 , created_date
	 , last_modified
  FROM Character_Flaws
 WHERE character_id = @character_id