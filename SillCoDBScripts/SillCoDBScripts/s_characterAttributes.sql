SELECT character_id
	 , attribute_id
	 , [value]
	 , modified_by
	 , created_date
	 , last_modified
  FROM Character_Attributes
 WHERE character_id = @character_id
;