drop table Character_Attributes;
drop table Character_Injuries;
drop table Character_Languages;
drop table Character_Perks;
drop table Character_Flaws;
drop table Character_Skills;
drop table Characters;
drop table Players;
drop table Languages;
drop table Skills;
drop table Attributes;
drop table Perks;
drop table Flaws;

/*Lookup Tables*/
create table Languages
(
	language_id int identity(1,1) primary key,
	[name] nvarchar(max),
	abbrev nvarchar(3),
	modified_by nvarchar(max) default 'scripts',
	created_date datetime default getdate(),
	last_modified datetime default getdate()
);

CREATE TABLE Attributes
(
	attribute_id nvarchar(4) PRIMARY KEY,
	[name] nvarchar(max),
	modified_by nvarchar(max) default 'scripts',
	created_date datetime default getdate(),
	last_modified datetime default getdate()
);

CREATE TABLE Skills
(
	skill_id int PRIMARY KEY,
	attribute_id nvarchar(4),
	skill_name nvarchar(max),
	skill_desc nvarchar(max),
	modified_by nvarchar(max) default 'scripts',
	created_date datetime default getdate(),
	last_modified datetime default getdate(),
	constraint FK_Skills_attribute_id foreign key(attribute_id) 
	           references Attributes(attribute_id)
			   on delete cascade 
			   on update cascade
);

create table Perks
(
	perk_id int identity(1,1) primary key,
	[name] nvarchar(max),
	[desc] nvarchar(max),
	[value] int,
	modified_by nvarchar(max) default 'scripts',
	created_date datetime default getdate(),
	last_modified datetime default getdate()
);

create table Flaws
(
	flaw_id int identity(1,1) primary key,
	[name] nvarchar(max),
	[desc] nvarchar(max),
	[value] int,
	modified_by nvarchar(max) default 'scripts',
	created_date datetime default getdate(),
	last_modified datetime default getdate()
);

/*Object tables*/
create table Players
(
	player_id int identity(1,1) primary key,
	name nvarchar(max),
	login nvarchar(max),
	password binary,
	modified_by nvarchar(max) default 'scripts',
	created_date datetime default getdate(),
	last_modified datetime default getdate()
);

CREATE TABLE Characters
(
	character_id int IDENTITY(1,1) PRIMARY KEY,
	player_id int,
	[name] nvarchar(max),
	profession nvarchar(max),
	nationality nvarchar(max),
	gender nvarchar(1),
	eye_color nvarchar(max),
	hair_color nvarchar(max),
	height nvarchar(max),
	[weight] nvarchar(max),
	handedness nvarchar(max),
	[desc] nvarchar(max),
	age int,
	experience int,
	[year] int,
	has_died bit default 0,
	is_npc bit default 0,
	modified_by nvarchar(max) default 'scripts',
	created_date datetime default getdate(),
	last_modified datetime default getdate(),
	constraint FK_Characters_player_id foreign key (player_id) 
	           references Players(player_id) 
			   on delete cascade 
			   on update cascade
);

create table Character_Injuries
(
	character_injury_id int identity(1,1) primary key,
	character_id int,
	injury_type nvarchar(max),
	modifier int,
	is_healed bit,
	modified_by nvarchar(max) default 'scripts',
	created_date datetime default getdate(),
	last_modified datetime default getdate(),
	constraint FK_Character_Injuries_character_id foreign key(character_id) 
			   references Characters(character_id)
			   on delete cascade 
			   on update cascade
);


/*Joining tables*/
create table Character_Languages
(
	character_id int,
	language_id int,
	modified_by nvarchar(max) default 'scripts',
	created_date datetime default getdate(),
	last_modified datetime default getdate(),
	constraint PK_Character_Languages primary key (character_id,language_id),
	constraint FK_Character_Languages_character_id foreign key(character_id) 
	           references Characters(character_id)
			   on delete cascade 
			   on update cascade,
	constraint FK_Character_Languages_language_id foreign key(language_id) 
	           references Languages(language_id)
			   on delete cascade 
			   on update cascade
);

CREATE TABLE Character_Attributes
(
	character_id int,
	attribute_id nvarchar(4),
	value int,
	modified_by nvarchar(max) default 'scripts',
	created_date datetime default getdate(),
	last_modified datetime default getdate(),
	constraint PK_Character_Attributes primary key (character_id,attribute_id),
	constraint FK_Character_Attributes_character_id foreign key(character_id) 
	           references Characters(character_id)
			   on delete cascade 
			   on update cascade,
	constraint FK_Character_Attributes_attribute_id foreign key(attribute_id) 
	           references Attributes(attribute_id)
			   on delete cascade 
			   on update cascade
);

CREATE TABLE Character_Skills
(
	character_id int,
	skill_id int,
	skill_points int,
	complexity int,
	specialization nvarchar(max),
	modified_by nvarchar(max) default 'scripts',
	created_date datetime default getdate(),
	last_modified datetime default getdate(),
	constraint PK_Character_Skills primary key (character_id,skill_id),
	constraint FK_Character_Skills_character_id foreign key(character_id) 
	           references Characters(character_id)
			   on delete cascade 
			   on update cascade,
	constraint FK_Character_Skills_skill_id foreign key(skill_id) 
	           references Skills(skill_id)
			   on delete cascade 
			   on update cascade
);

create table Character_Perks
(
	character_id int,
	perk_id int,
	modified_by nvarchar(max) default 'scripts',
	created_date datetime default getdate(),
	last_modified datetime default getdate(),
	constraint PK_Character_Perks primary key (character_id,perk_id),
	constraint FK_Character_Perks_character_id foreign key(character_id) 
	           references Characters(character_id)
			   on delete cascade 
			   on update cascade,
	constraint FK_Character_Perks_perk_id foreign key(perk_id) 
	           references Perks(perk_id)
			   on delete cascade 
			   on update cascade
);

create table Character_Flaws
(
	character_id int,
	flaw_id int,
	modified_by nvarchar(max) default 'scripts',
	created_date datetime default getdate(),
	last_modified datetime default getdate(),
	constraint PK_Character_Flaws primary key (character_id,flaw_id),
	constraint FK_Character_Flaws_character_id foreign key(character_id) 
	           references Characters(character_id)
			   on delete cascade 
			   on update cascade,
	constraint FK_Character_Flaws_perk_flaw_id foreign key(flaw_id) 
	           references Flaws(flaw_id)
			   on delete cascade 
			   on update cascade
);


/*
CREATE TABLE Items
(
	item_id int not null PRIMARY KEY,
	item_type_id int,
	name nvarchar(max),
	weight float,
	armor int,
	armor_area nvarchar(max),
	complexity int,
	note nvarchar(max),
)
*/