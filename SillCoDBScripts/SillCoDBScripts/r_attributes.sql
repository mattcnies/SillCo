SELECT attribute_id
     , [name]
	 , abbrev
	 , modified_by
	 , created_date
	 , last_modified
  FROM Attributes;